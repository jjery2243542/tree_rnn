import sys
import xml.etree.ElementTree as ET

path=sys.argv[1]
tree = ET.parse(path)
root = tree.getroot()
with open(sys.argv[2], "a") as fout:
    for token in root.iter("token"):
        fout.write(token[0].text.encode("utf-8") + " ")
    fout.write("\n")
    
