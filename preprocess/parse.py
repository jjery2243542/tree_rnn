from collections import deque

class stack(object):
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

def get_tagset(fname="./tagset"):
    tagset = {}
    idx = 0
    with open(fname, "r") as fin:
        for line in fin:
            tagset[line.strip()]  = idx 
            idx += 1
    return tagset

def recursive_func(s, start, end, parent, tree, match):
    #include start, exclude end
    if start == end:
        return
    inner_str = s[start:end]
    node = {}
    num_nodes = len(tree)
    node["index"] = num_nodes
    node["parent"] = parent
    pos = inner_str[1:inner_str.find(" ")]
    node["pos"] = pos
    if inner_str[inner_str.find(" ") + 1] not in ["(", ")"]:
        word = inner_str[inner_str.find(" ") + 1:inner_str.find(")")]
        node["word"] = word.lower()
    else:
        node["word"] = None
    node["children"] = []
    tree.append(node)
    i = start + 1
    while i < end:
        if match[i] != -1 and match[i] != start:
            node["children"].append(len(tree))
            recursive_func(s, i, match[i] + 1, node["index"], tree, match)
            i = match[i] + 1
        else:
            i += 1

    
def match(s):
    S = stack()
    num_nodes = 0
    pare_match = [-1 for x in range(len(s))]
    for i, c in enumerate(s):
        if c == "(":
            S.push(i)
        elif c == ")":
            match_idx = S.pop()
            pare_match[match_idx] = i
            pare_match[i] = match_idx
            num_nodes += 1
    return pare_match, num_nodes

def bottomup_traverse(tree):
    new_tree = []
    idx = 0
    added_node = set()
    q = deque()
    for node in tree:
        if node["word"] != None:
            node["bottomup_index"] = idx
            idx += 1
            new_tree.append(node)
            added_node.add(node["index"])
            q.append(node["index"])
    while q:
        node = tree[q.popleft()]
        parent_idx = node["parent"]
        if parent_idx not in added_node and parent_idx != -1:
            p_node = tree[parent_idx]
            p_node["bottomup_index"] = idx
            idx += 1
            new_tree.append(p_node)
            q.append(parent_idx)
            added_node.add(parent_idx)
    return new_tree  
        
def parse(s):
    tree = []
    pare_match, num_nodes = match(s)
    # remove the outer parenthsis
    recursive_func(s, 6, len(s) - 1, -1, tree, pare_match)
    new_tree = bottomup_traverse(tree)
    return new_tree, tree

def get_parent(tree, old_tree):
    parents = []
    for node in tree:
        p_idx = node["parent"]
        if p_idx == -1:
            parents.append(0)
        else:
            parents.append(old_tree[p_idx]["bottomup_index"] + 1)
    return parents

def get_pos(tree, tagset):
    tags = []
    for node in tree:
        try:
            tags.append(tagset[node["pos"]])
        except:
            tags.append(tagset["MARK"])
    return tags

def get_data(parse_tree_str):
    tree, old_tree = parse(parse_tree_str)
    tagset = get_tagset()
    parents = get_parent(tree, old_tree)
    labels = get_pos(tree, tagset)
    return parents, labels
if __name__ == "__main__":
    parse_tree="(ROOT (S (NP (CD Two) (NNS dogs)) (VP (VBP are) (VP (VBG fighting)))))"
    for node in parse(parse_tree)[1]:
        #print node
        1+1
