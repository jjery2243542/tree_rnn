from parse import get_data
import sys
from itertools import izip
path_in = sys.argv[1]
path_parents = sys.argv[2]
path_labels = sys.argv[3]
with open(path_in, "r") as infile:
    with open(path_parents, "w") as parents_file:
        with open(path_labels, "w") as labels_file:
            for line in infile:
                parents, labels = get_data(line.strip())
                for p, l in izip(parents, labels):
                    parents_file.write(str(p) + " ")
                    labels_file.write(str(l) + " ")
                parents_file.write("\n")
                labels_file.write("\n")
                
        
