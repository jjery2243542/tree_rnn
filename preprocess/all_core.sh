#tmpFile=tmp
#tmpOutput=tmp.out
#treeFile=(../treelstm/data/sick/pos_train/a.txt  ../treelstm/data/sick/pos_train/b.txt ../treelstm/data/sick/pos_dev/a.txt../treelstm/data/sick/pos_dev/b.txt ../treelstm/data/sick/pos_test/a.txt ../treelstm/data/sick/pos_test/b.txt)
#inputFile=(../treelstm/data/sick/train/a.txt ../treelstm/data/sick/train/b.txt ../treelstm/data/sick/dev/a.txt ../treelstm/data/sick/dev/b.txt ../treelstm/data/sick/test/a.txt ../treelstm/data/sick/test/b.txt)
#for i in 0 1 2 3 4 5
#do
#while read p; do
##echo $p
#echo $p > $tmpFile
#./corenlp.sh -annotators tokenize,ssplit,parse -file $tmpFile 2>/dev/null
#python xml_parser.py $tmpOutput ${treeFile[$i]}
#done < ${inputFile[$i]}
#done
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/train/b.txt -outputDirectory ../treelstm/data/sick/pos_train/
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/train/a.txt -outputDirectory ../treelstm/data/sick/pos_train/
python xml_parser.py ../treelstm/data/sick/pos_train/a.txt.out ../treelstm/data/sick/pos_train/a.txt.tree
python xml_parser.py ../treelstm/data/sick/pos_train/b.txt.out ../treelstm/data/sick/pos_train/b.txt.tree
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/dev/b.txt -outputDirectory ../treelstm/data/sick/pos_dev/
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/dev/a.txt -outputDirectory ../treelstm/data/sick/pos_dev/
python xml_parser.py ../treelstm/data/sick/pos_dev/a.txt.out ../treelstm/data/sick/pos_dev/a.txt.tree
python xml_parser.py ../treelstm/data/sick/pos_dev/b.txt.out ../treelstm/data/sick/pos_dev/b.txt.tree
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/test/b.txt -outputDirectory ../treelstm/data/sick/pos_test/
./corenlp.sh -annotators tokenize,ssplit,parse -file ../treelstm/data/sick/test/a.txt -outputDirectory ../treelstm/data/sick/pos_test/
python xml_parser.py ../treelstm/data/sick/pos_test/a.txt.out ../treelstm/data/sick/pos_test/a.txt.tree
python xml_parser.py ../treelstm/data/sick/pos_test/b.txt.out ../treelstm/data/sick/pos_test/b.txt.tree
